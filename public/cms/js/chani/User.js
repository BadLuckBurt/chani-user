
var chaniUser = new Class({
	Extends: chaniBlueprintCms,
	initialize: function(options) {
		this.setOptions(options);
		this.prepareUserList();
		if(this.options.id == 0) {
			var userList = $('manageusers');
			Chani.toggleCMSContent(userList);
		}
	},
	prepareUserList: function() {
		var self = this;
		this.overview = $('userOverview');
		this.overview.addEventListener('click',function(event) {
			var target = event.target;
			if(target.tagName.toLowerCase() == 'i') {
				var parent = $(target.parentNode);
				if(parent.hasClass('user-delete')) {
					this.deleteUser(parent);
				}
			}
			return false;
		}.bind(self));
	},
	deleteUser: function(el) {
		if(!confirm(el.getAttribute('data-confirm'))) {
			return false;
		}
		var id = el.getAttribute('data-id');
		var deletePage = new Request({
			url: this.options.ajax.deleteUserUrl + id,
			data: {
				id: id
			},
			onSuccess: function(txt) {
				this.replaceOverview(txt);
			}.bind(this)
		});
		deletePage.send();
	},
	replaceOverview: function(txt) {
		var ul = Element('ul');
		ul.id = 'userOverview';
		ul.innerHTML = txt;
		this.overview.parentNode.replaceChild(ul, this.overview);
		this.overview = null;
		this.prepareUserList();
	}
});