<?php

	namespace User\Models\Chani;
	use \Core\Models\Chani\CmsBlueprint;
	use \Core\Shared AS Shared;

	class CmsUser extends CmsBlueprint {

		public $_model ='User';
		public $dtCreated;
		public $dtUpdated;
		public $sUserName;
		public $sPassword;
		public $sDecryptedPassword;
		public $sEmail;

		public function getSource() {
			return 'user';
		}

		public function initialize() {
			$sClass = get_class($this);
			parent::initialize();
		}

		//TODO: Check this function to see if it works properly
		//Create a new user
		public static function add() {
			$oUser = new User();
			$oUser->dtCreated = Shared::getDBDate();
			$oUser->dtUpdated = 0;
			$oUser->sUserName = '';
			$oUser->sPassword = '';
			$oUser->sEmail = '';
			if($oUser->saveData() == false) {
				echo('error saving user');
				die;
			} else {
				return $oUser;
			}
		}
	}