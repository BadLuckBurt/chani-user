<?php
	namespace User;
	use Phalcon\Loader,
	Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter,
	Phalcon\Mvc\Dispatcher,
	Phalcon\Mvc\View\Simple,
	Phalcon\Mvc\ModuleDefinitionInterface,
	Phalcon\Events\Manager;

	class Module extends \Core\ModuleTemplate
	{
		/**
		 * Register specific services for the module
		 */
		public function registerServices($oDi,$sNameSpace = '', $bCore = false)
		{
			parent::registerServices($oDi,__NAMESPACE__,$bCore);
		}
	}
