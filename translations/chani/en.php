<?php

	$aMessages = array(
		'module' => 'Users',
		'user' => 'User',
		'add' => 'Add user',
		'edit' => 'Edit user',
		'delete' => 'Delete user',
		'cancel' => 'Cancel',
		'sUserName'    =>'Username',
		'sPassword' => 'Password',
		'sDecryptedPassword' => 'Password',
		'sEmail'   => 'Email',
		'saveForm'  => 'Save',
		'overviewTitle' => 'Users',
		'confirmDelete' => 'Do you want to remove user'
	);