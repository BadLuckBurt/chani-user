<?php

	$aMessages = array(
		'module' => 'Gebruikers',
		'user' => 'Gebruiker',
		'add' => 'Voeg gebruiker toe',
		'edit' => 'Bewerk gebruiker',
		'delete' => 'Verwijder gebruiker',
		'cancel' => 'Annuleren',
		'sUserName'    =>'Gebruikersnaam',
		'sPassword' => 'Wachtwoord',
		'sDecryptedPassword' => 'Wachtwoord',
		'sEmail'   => 'E-mailadres',
		'saveForm'  => 'Opslaan',
		'overviewTitle' => 'Gebruikers',
		'confirmDelete' => 'Wil je deze gebruiker verwijderen: '
	);