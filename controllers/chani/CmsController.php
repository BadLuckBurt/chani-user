<?php

	//TODO: Integrate controller and module into dashboard

	namespace User\Controllers\Chani;
	use \Core\Controllers\Chani\CmsController AS Controller;
	use \Core\Shared AS Shared;
	use \User\Models\Chani\CmsUser;
	use \Phalcon\Security AS Security;
	use \Page\Controllers\Chani\CmsController AS PageController;

	class CmsController extends Controller {

		public $aButtons = array(
			'bAdd' => false,
			'bEdit' => true,
			'bDelete' => true,
			'bMove' => false
		);

		public $aMeta = array(
			'name' => array(
				'charset' => 'UTF-8',
				'keywords'=> 'your, tags',
				'description' => '150 words',
				'language' => 'NL',
				'robots' => 'index,follow'
			),
			'http-equiv' => array(
				'Expires' => 0,
				'Pragma' => 'no-cache',
				'Cache-Control' => 'no-cache',
				'imagetoolbar' => 'no',
				'x-dns-prefetch-control' => 'off'
			)
		);

		public function initialize() {
			$this->aModules['user']['class'] = 'active';
			$this->aScripts[] = 'public/modules/user/cms/js/chani/User.js';
			$this->aScripts[] = 'public/core/js/cms/chani/ImageCropper.js';
		}

		public $aFormFields = array(
			array(
				'type' => 'twoCol',
				'fields' => array(
					'sUserName' => array(
						'tag' => 'textField',
						'value' => '',
						'data-locale' => 'false',
						'class' => 'user'
					)
				)
			),
			array(
				'type' => 'twoCol',
				'fields' => array(
					'sDecryptedPassword' => array(
						'tag' => 'passwordField',
						'value' => '',
						'data-locale' => 'false',
						'class' => 'user'
					)
				)
			),
			array(
				'type' => 'twoCol',
				'fields' => array(
					'sEmail' => array(
						'tag' => 'textField',
						'value' => '',
						'data-locale' => 'false',
						'class' => 'user'
					)
				)
			)
		);

		public function indexAction() {
			$this->aDomready[] = "
				oChaniObjects['user'] = new chaniUser({
					id: 0,
					ajax: {
						deleteUserUrl: '/chani/user/delete/'
					}
				});	
			";
			$oTranslator = $this->_getTranslation(__NAMESPACE__);
			$sUserList = $this->getUserList();
			$dbContents = ['manageusers' => $sUserList];
			$sBody = '';
			$sBody = $this->renderBody($sBody, $dbContents);
			$sHead = $this->renderHead();
			$sHTML = $this->renderHTML($oTranslator->_('overviewTitle'), $sHead, $sBody, true);
			echo($sHTML);
		}

		//View a user profile
		public function profileAction() {
			$aParams = $this->dispatcher->getParams();
			if(count($aParams) === 0) {
				echo('no user id given');
			} else {
				$id = $aParams[0];
				$oUser = CmsUser::findFirst($id);
				if($oUser == false) {
					echo('User has been deleted');
					die;
				} else {
					$oTranslator = $this->_getTranslation(__NAMESPACE__);
					$sBody = $this->view->render('chani/profile', array(
						'user' => $oUser,
						't' => $oTranslator
					));
					$sBody = $this->renderBody($sBody);
					$sHead = $this->renderHead();
					$sTitle = $oTranslator->_('user').': '.$oUser->sUserName;
					$sHTML = $this->renderHTML($sTitle, $sHead, $sBody);
					echo($sHTML);
				}
			}
		}

		//Create a new user
		public function addAction() {
			$aParams = $this->dispatcher->getParams();

			$oUser = CmsUser::add();

			$this->response->redirect('chani/'.$oUser->_modelLower.'/edit/'.$oUser->id);
		}

		//Edit a user
		public function editAction() {
			$aParams = $this->dispatcher->getParams();

			if(count($aParams) === 0) {

				echo('No user id found');
				die;

			} else {
				$oTranslator = $this->_getTranslation(__NAMESPACE__);

				$id = $aParams[0];
				$oUser = CmsUser::findFirst($id);
				if($oUser == false) {
					echo('User has been deleted');
					die;
				}

				$this->aDomready[] = "
				oChaniObjects['user'] = new chaniUser({
					id: $id,
					ajax: {
						deleteUserUrl: 'chani/user/delete/'
					}
				});	
				";

				$dashboardModals = [];

				$oSecurity = new Security();

				$sTitle = $oUser->sUserName;
				$oUser->sDecryptedPassword = '';

				$oEditables = [$oUser];
				$this->prepareFormFields($oUser, $oEditables, $oTranslator);

				$sBody = $this->view->render('chani/form',array(
					'formFields' => $this->aFormFields,
					'user' => $oUser,
					't' => $oTranslator
				));
				$cPageController = new PageController();
				$sitemapRender = $cPageController->renderSitemap();
				$sBody = $this->renderBody($sBody, ['sitemap' => $sitemapRender]);
				$sHead = $this->renderHead();
				$sHTML = $this->renderHTML($sTitle, $sHead, $sBody, true);
				echo($sHTML);

			}
		}

		//Save changed user values
		public function saveAction() {
			//Create an instance
			$iUserId = $this->request->getPost('id',null,0);
			if($iUserId === 0) {
				echo('no user id in post');
				die;
			} else {
				$sUserName = $this->request->getPost('sUserName',null,'');
				if($sUserName == '') {
					echo('username is empty string :/');
					die;
				}

				$sEmail = $this->request->getPost('sEmail',null,'');
				if($sEmail == '') {
					echo('email is empty string :/');
					die;
				}
				$oUser = CmsUser::findFirst($iUserId);
				$oUser->dtUpdated = Shared::getDBDate();
				$oUser->sUserName = $sUserName;
				$oUser->sEmail = $sEmail;

				$sPassword = $this->request->getPost('sDecryptedPassword',null,'');
				if($sPassword !== '') {
					$oSecurity = new Security();
					$sEncryptedPW = $oSecurity->hash($sPassword);
					$oUser->sPassword = $sEncryptedPW;
				}

				if($oUser->saveData() == false) {
					echo('error saving user');
					die;
				} else {
					$this->response->redirect('chani/user/');
				}

			}

		}

		//Delete a user
		public function deleteAction() {
			$iUserId = $this->request->getPost('id',null,0);
			if($iUserId === 0) {
				echo('no user id in post');
				die;
			} else {
				$aUserList = $this->buildOverview();
				$sOverview = '';
				$oTranslator = $this->_getTranslation(__NAMESPACE__);
				$this->swapViewDir();
				for($i = 0; $i < count($aUserList); $i++) {
					$sOverview .= $this->renderOverviewRow($aUserList[$i], $this->aButtons, 1, $oTranslator, 'user', []);
				}
				$this->restoreViewDir();
				echo($sOverview);
				die;
			}
		}

		public function getUserList() {
			$t = $this->_getTranslation(__NAMESPACE__);
			$aUsers = $this->buildOverview();

			$sUserList = $this->renderOverview($aUsers, 'user', $t, $this->aButtons);
			return $sUserList;
		}
		
		public function buildOverview() {
			$oUsers = CmsUser::find([
				'order' => 'sUserName ASC'
			]);

			$oTranslator = $this->_getTranslation(__NAMESPACE__);
			$aUsers = [];
			for($i = 0; $i < count($oUsers); $i++) {
				$aUsers[] = [
					'id' => $oUsers[$i]->id,
					'htmlId' => 'user-'.$oUsers[$i]->id,
					'data-target' => '',
					'sTitle' => $oUsers[$i]->sUserName,
					'class' => '',
					'sub' => [],
					'editUrl' => 'chani/user/edit/'.$oUsers[$i]->id,
					'deleteUrl' => 'chani/user/delete/'.$oUsers[$i]->id,
					'confirmDelete' => $oTranslator->_('confirmDelete').' '.$oUsers[$i]->sUserName
				];
			}
			return $aUsers;
		}
	}