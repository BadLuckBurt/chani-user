<div class="chaniUserProfile">
	<div class="chaniUserInfo">
		<div class="uniForm">
			<div class="col">
				<div class="twoCol">
					<div class="chaniUserThumbnail">

					</div>
				</div>
				<div class="twoCol"><label>Username</label> <span class="uniFormSpan">{{user.sUserName}}</span></div>
				<div class="twoCol"><label>Email</label> <span class="uniFormSpan">{{user.sEmail}}</span></div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="formButtons">
			<div class="buttons">
				<a href="/chani/user/edit/{{user.id}}" class="button right">{{t._('edit')}}</a>
				<a href="/chani/user/delete/{{user.id}}" class="button left">{{t._('delete')}}</a>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</div>