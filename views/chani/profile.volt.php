<div class="chaniUserProfile">
	<div class="chaniUserInfo">
		<div class="uniForm">
			<div class="col">
				<div class="twoCol">
					<div class="chaniUserThumbnail">

					</div>
				</div>
				<div class="twoCol"><label>Username</label> <span class="uniFormSpan"><?php echo $user->sUserName; ?></span></div>
				<div class="twoCol"><label>Email</label> <span class="uniFormSpan"><?php echo $user->sEmail; ?></span></div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="formButtons">
			<div class="buttons">
				<a href="/chani/user/edit/<?php echo $user->id; ?>" class="button right"><?php echo $t->_('edit'); ?></a>
				<a href="/chani/user/delete/<?php echo $user->id; ?>" class="button left"><?php echo $t->_('delete'); ?></a>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</div>