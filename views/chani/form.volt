<div data-target="user-blocks" id="dashboard-content-toolbar" class="dashboard-content-toolbar">
    <div id="dashboard-content-settings-wrapper" class="dashboard-content-settings-wrapper">
        <div class="dashboard-content-settings">
        <h1>Edit user</h1>
        {{ form("chani/user/save/"~user.id, "method": "post",'id':'cmsForm') }}
        {{  hiddenField(['id': 'id','name':'id','value':user.id])}}
        <div class="uniForm">
            {% for columns in formFields %}
            <div class="col">
                {% for key, field in columns['fields'] %}
                <div class="{{ columns['type'] }}">
                    <label for="{{ key }}">{{ field['label'] }}</label>
                    {{ field['html'] }}
                </div>
                {% endfor %}
            </div>
            {% endfor %}
            <div class="clear"></div>
        </div>
        <div class="formButtons">
            <div class="buttons">
                <button id="saveForm" type="submit" name="saveForm" class="user-save save floatRight">{{t._('saveForm') }}</button>
                <a href="/chani/user/" class="cancel button">{{t._('cancel')}}</a>
            </div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
        {{ endForm() }}
        </div>
    </div>
</div>