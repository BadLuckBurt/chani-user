<div data-target="user-blocks" id="dashboard-content-toolbar" class="dashboard-content-toolbar">
    <div id="dashboard-content-settings-wrapper" class="dashboard-content-settings-wrapper">
        <div class="dashboard-content-settings">
        <h1>Edit user</h1>
        <?php echo $this->tag->form(array('chani/user/save/' . $user->id, 'method' => 'post', 'id' => 'cmsForm')); ?>
        <?php echo $this->tag->hiddenfield(array('id' => 'id', 'name' => 'id', 'value' => $user->id)); ?>
        <div class="uniForm">
            <?php foreach ($formFields as $columns) { ?>
            <div class="col">
                <?php foreach ($columns['fields'] as $key => $field) { ?>
                <div class="<?php echo $columns['type']; ?>">
                    <label for="<?php echo $key; ?>"><?php echo $field['label']; ?></label>
                    <?php echo $field['html']; ?>
                </div>
                <?php } ?>
            </div>
            <?php } ?>
            <div class="clear"></div>
        </div>
        <div class="formButtons">
            <div class="buttons">
                <button id="saveForm" type="submit" name="saveForm" class="user-save save floatRight"><?php echo $t->_('saveForm'); ?></button>
                <a href="/chani/user/" class="cancel button"><?php echo $t->_('cancel'); ?></a>
            </div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
        <?php echo $this->tag->endform(); ?>
        </div>
    </div>
</div>